# Static Code Policy Enforcement Terraform

### Goal
Enforce policy on terraform statically prior to deployment to environment

### how to use
##### Testing
This is an mvp product and has only been tested in Jenkins.  This script should be usable across all CI/CD pipelines while deploying Terraform

##### Usage
- configure your repository in jenkins
- add a build step that runs the bash.sh commands
- install the python pluggin for Jenkins
- add a build step that runs the enforce.py script
- if your using a nested repository, change the path variable to the location of your Terraform

let it rip
