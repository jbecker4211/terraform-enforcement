import hcl
import json
import re
import os
import glob

dirname = os.path.dirname(__file__)

#path = os.path.join(dirname, 'start-scripts')
path = 'start-scripts/'
files = []

print(path+ "+++++++")

for r, d, f in os.walk(path):
  for file in f:
      files.append(os.path.join(r,file))

print(files)




for file in files:
    print(file)
    with open(file, 'r') as fp:
        try:
            obj = hcl.load(fp)
        except:
            "no hcl was found in file " + "fp"
        for resourceType , load  in  obj.items():
            if resourceType == "resource":
                print('resource was found')
                exit(1)
            elif resourceType == "module":
                for module, package in load.items():
                    if not re.match("git::https://bitbucket.org/cloudreach/...",package.get("source")):
                        print("please use company modules")
                        exit(1)
